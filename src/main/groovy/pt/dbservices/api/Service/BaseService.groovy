package pt.dbservices.api.Service

import groovyx.net.http.ContentType
import groovyx.net.http.RESTClient
import pt.dbservices.api.Helper.PropertyHelper

class BaseService {

    RESTClient restClient

    BaseService(apiName)
    {
        def url = new PropertyHelper().getServiceBaseUrl(apiName)
        restClient = new RESTClient(url, ContentType.JSON)
    }

    def get(path, query = [:]){
        return restClient.get(path: path, query: query)
    }
}
