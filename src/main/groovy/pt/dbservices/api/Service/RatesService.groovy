package pt.dbservices.api.Service

import pt.dbservices.api.Helper.DateHelper

class RatesService extends BaseService {

    def query

    RatesService() {
        super('ratesapi')
        query = [:]
    }

    def setQuery(base = null, symbols = null){
        if (base)
            query.put('base', base)
        if (symbols) {
            def currencies = ""
            symbols.each { it ->
                currencies += it
                if (it != symbols.last())
                    currencies += ","
            }
            query.put('symbols', currencies)
        }
    }

    def getLatest(base = null, symbols = null) {
        setQuery(base, symbols)
        super.get('latest', query)
    }

    def getRatesAtDate(Date requestDate, base = null, symbols = null) {
        setQuery(base, symbols)
        super.get(DateHelper.dateToFormatYearMonthDay(requestDate),query)
    }

}
