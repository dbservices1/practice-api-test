package pt.dbservices.api.Helper

class  PropertyHelper {

    def config

    PropertyHelper(){
        Properties properties = new Properties()
        def pathName = System.getProperty("user.dir") + "/src/test/resources/config.properties"
        File propsFile = new File(pathName)
        properties.load(propsFile.newDataInputStream())
        config = new ConfigSlurper().parse(properties)
    }

    def getServiceBaseUrl(String serviceName) {
        return config.api.get(serviceName).base.url
    }

}
