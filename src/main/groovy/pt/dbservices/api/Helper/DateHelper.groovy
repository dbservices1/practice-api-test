package pt.dbservices.api.Helper

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.DayOfWeek

class DateHelper {

    static def dateToFormatYearMonthDay(Date dateToFormat) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd")
        return formatter.format(dateToFormat)
    }

    static def stringDayMonthYearToDate(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy")
        Date date = formatter.parse(dateString)
        return date
    }

    static def getLastWeekDateFromDate(Date fromDate) {
        Calendar calendar = Calendar.getInstance()
        calendar.setTime(fromDate)
        switch (fromDate.toDayOfWeek()) {
            case DayOfWeek.SATURDAY:
                calendar.add(Calendar.DATE, -1);
                break
            case DayOfWeek.SUNDAY:
                calendar.add(Calendar.DATE, -2);
                break
            default:
                break
        }
        return calendar.getTime();
    }
}
