package pt.dbservices.api.test.Rates

import pt.dbservices.api.Helper.DateHelper
import pt.dbservices.api.Service.RatesService
import spock.lang.Specification

import java.time.DayOfWeek

class PastConversionSpec extends Specification{

    def "Get the rate for all currencies at specific business day #dateRequested"(dateRequested){
        given: "that i want to get all rates at specific date"
        RatesService ratesService = new RatesService()
        Date date = DateHelper.stringDayMonthYearToDate(dateRequested)

        when: "i ask for all the last rates"
        def response = ratesService.getRatesAtDate(date)

        then: "get the rates is returned with sucess"
        assert response.status == 200
        def bodyResponse = response.data
        assert bodyResponse.rates.size() > 0

        and: "at date #dateRequested"
        assert bodyResponse.date == DateHelper.dateToFormatYearMonthDay(date)

        and: "the default currency is EUR"
        assert bodyResponse.base == "EUR"

        where:
        dateRequested   | _
        "12/01/2010"    | _
        "15/12/2020"    | _
        "08/01/2021"    | _

    }

    def "Get the rate for all currencies at last week day"(dateRequested){
        given: "that i want to get all rates at specific date"
        RatesService ratesService = new RatesService()
        Date date = DateHelper.stringDayMonthYearToDate(dateRequested)

        when: "i ask for all the last rates"
        def response = ratesService.getRatesAtDate(date)

        then: "get the rates is returned with sucess"
        assert response.status == 200
        def bodyResponse = response.data
        assert bodyResponse.rates.size() > 0

        and: "at last week date"
        assert bodyResponse.date == DateHelper.dateToFormatYearMonthDay(DateHelper.getLastWeekDateFromDate(date))


        and: "the default currency is EUR"
        assert bodyResponse.base == "EUR"

        where:
        dateRequested   | _
        "09/01/2021"    | _
        "10/01/2021"    | _

    }

    def "Get the rate for all currencies in a different base currency at specific date"(baseCurrency){
        given: "that i want to get all rates for today"
        RatesService ratesService = new RatesService()
        Date date = DateHelper.stringDayMonthYearToDate("12/01/2010")

        when: "i ask for all the last rates with a specific base  currency"
        def response = ratesService.getRatesAtDate(date,baseCurrency)

        then: "get the rates is returned with sucess"
        assert response.status == 200
        def bodyResponse = response.data
        assert bodyResponse.rates.size() > 0

        and: "at date"
        assert bodyResponse.date == DateHelper.dateToFormatYearMonthDay(date)

        and: "the default currency is EUR"
        assert bodyResponse.base == baseCurrency

        where:
        baseCurrency    | _
        'BRL'           | _
        'USD'           | _
        'PHP'           | _

    }

    def "Get the rate for specific currencies at specific date"(currenciesList) {
        given: "that i want to get all rates for today"
        RatesService ratesService = new RatesService()
        Date date = DateHelper.stringDayMonthYearToDate("12/01/2010")

        when: "i ask for specific last rates"
        def response = ratesService.getRatesAtDate(date, null, currenciesList)

        then: "get the rates is returned with sucess"
        assert response.status == 200
        def bodyResponse = response.data
        assert bodyResponse.rates.size() == currenciesList.size()

        and: "only the ones i requested"
        bodyResponse.rates.each { rate ->
            assert currenciesList.find({ it == rate.key })
        }

        and: "at date"
        assert bodyResponse.date == DateHelper.dateToFormatYearMonthDay(date)

        where:
        currenciesList | _
        ['BRL']        | _
        ['BRL', 'USD'] | _
        ['HUF', 'BRL', 'USD', 'ZAR'] | _
    }
}
