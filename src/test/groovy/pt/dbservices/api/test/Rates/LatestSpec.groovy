package pt.dbservices.api.test.Rates

import pt.dbservices.api.Helper.DateHelper
import pt.dbservices.api.Service.RatesService
import spock.lang.Specification

class LatestSpec extends Specification{

    def "Get the rate for all currencies for today"(){
        given: "that i want to get all rates for today"
        RatesService ratesService = new RatesService()
        Date todayDate = new Date()

        when: "i ask for all the last rates"
        def response = ratesService.getLatest()

        then: "get the rates is returned with sucess"
        assert response.status == 200
        def bodyResponse = response.data
        assert bodyResponse.rates.size() > 0

        and: "at today"
        assert bodyResponse.date == DateHelper.dateToFormatYearMonthDay(todayDate)

        and: "the default currency is EUR"
        assert bodyResponse.base == "EUR"

    }

    def "Get the rate for all currencies in #baseCurrency base currency"(baseCurrency){
        given: "that i want to get all rates for today"
        RatesService ratesService = new RatesService()
        Date todayDate = new Date()

        when: "i ask for all the last rates with #baseCurrency base currency"
        def response = ratesService.getLatest(baseCurrency)

        then: "get the rates is returned with sucess"
        assert response.status == 200
        def bodyResponse = response.data
        assert bodyResponse.rates.size() > 0

        and: "at today"
        assert bodyResponse.date == DateHelper.dateToFormatYearMonthDay(todayDate)

        and: "the default currency is EUR"
        assert bodyResponse.base == baseCurrency

        where:
        baseCurrency    | _
        'BRL'           | _
        'USD'           | _
        'PHP'           | _

    }

    def "Get the rate for specific currencies"(currenciesList) {
        given: "that i want to get all rates for today"
        RatesService ratesService = new RatesService()
        Date todayDate = new Date()

        when: "i ask for specific last rates"
        def response = ratesService.getLatest(null, currenciesList)

        then: "get the rates is returned with sucess"
        assert response.status == 200
        def bodyResponse = response.data
        assert bodyResponse.rates.size() == currenciesList.size()

        and: "only the ones i requested"
        bodyResponse.rates.each { rate ->
            assert currenciesList.find({ it == rate.key })
        }

        and: "at today"
        assert bodyResponse.date == DateHelper.dateToFormatYearMonthDay(todayDate)


        where:
        currenciesList | _
        ['BRL']        | _
        ['BRL', 'USD'] | _
        ['HUF', 'BRL', 'USD', 'ZAR'] | _
    }
}
